function count() {
    var a = document.getElementById("t1");
    var b = +a.value;
    var nop = 0;
    var non = 0;
    var noz = 0;
    while (b > 0) {
        var num;
        num = prompt("Enter a number");
        if (num === null) {
            alert("No number entered. Try again.");
            continue;
        }
        if (isNaN(+num)) {
            alert("Invalid number entered. Try again.");
            continue;
        }
        if (+num > 0) {
            nop++;
        }
        else {
            if (+num < 0)
                non++;
            else {
                if (+num == 0)
                    noz++;
            }
        }
        b--;
    }
    document.getElementById("data").innerHTML = "<b>the no of positive negative and zero respectively </b>" + nop + "   " + non + "   " + noz;
}
