function type1() {
    var a = document.getElementById("co1");
    var b = document.getElementById("co2");
    var c = document.getElementById("co3");
    var side1 = +a.value;
    var side2 = +b.value;
    var side3 = +c.value;
    var flg = 0;
    if ((Math.pow(side1, 2) + Math.pow(side2, 2) == Math.pow(side3, 2)) || (Math.pow(side2, 2) + Math.pow(side3, 2) == Math.pow(side1, 2)) || (Math.pow(side1, 2) + Math.pow(side3, 2) == Math.pow(side2, 2)))
        flg = 1;
    console.log(flg);
    if (side1 == side2 && side2 == side3) {
        document.getElementById("ans").innerHTML = "EQUILATERAL";
    }
    else if (side1 == side2 || side1 == side3 || side2 == side3) {
        if (flg == 0)
            document.getElementById("ans").innerHTML = "Isosceles triangle.";
        else {
            if (flg == 1)
                document.getElementById("ans").innerHTML = "Isosceles triangle and Right angled Triangle";
        }
    }
    else {
        if (flg == 0)
            document.getElementById("ans").innerHTML = "Scalene triangle.";
        else {
            if (flg == 1)
                document.getElementById("ans").innerHTML = "Scalene triangle and Right angled Triangle";
        }
    }
}
